import torch
import torchvision
import os
import cv2
import time
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
from scipy.ndimage.measurements import label
from torch import nn
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torchvision import transforms
from torchvision.utils import save_image
from torchvision.datasets import MNIST
from multiprocessing import Pool, freeze_support
from slackIntegration import logSlack
from itertools import product
from torchsummary import summary
from torchviz import make_dot

# main_image_directory = "../AIA_Images/mini_batch"
main_image_directory = "../AIA_Images"
active_region_path_directory = "{directory}/AR".format(directory=main_image_directory)
threshold_value = 180

# %% autoencoder
class autoencoder(nn.Module):
    def __init__(self):
        super(autoencoder, self).__init__()
        self.conv1 = nn.Sequential(
           nn.Conv1d(in_channels=1, out_channels=64, kernel_size=2, padding=0),
           nn.ReLU(inplace=True),
           nn.MaxPool1d(kernel_size=2,  return_indices=True, padding=0)          
        )
        self.conv2 = nn.Sequential(
           nn.Conv1d(in_channels=64, out_channels=32, kernel_size=2, padding=0),
           nn.ReLU(inplace=True),
           nn.MaxPool1d(kernel_size=2,  return_indices=True, padding=0)          
        )
        self.conv3 = nn.Sequential(
           nn.Conv1d(in_channels=32, out_channels=1, kernel_size=2, padding=0),
           nn.ReLU(inplace=True),
           nn.MaxPool1d(kernel_size=2,  return_indices=True, padding=0)          
        )     
        
        self.unpool1 = nn.MaxUnpool1d(kernel_size=2, padding=0)
        self.deconv1 = nn.Sequential(
            nn.ConvTranspose1d(in_channels=1, out_channels=32 ,kernel_size=2, padding=0,output_padding=0),
            nn.ReLU(inplace=True),
            nn.ConstantPad1d( padding=(0,1), value = 0),            
            # nn.Tanh()
        )
        self.unpool2 = nn.MaxUnpool1d(kernel_size=2, padding=0)
        self.deconv2 = nn.Sequential(
            nn.ConvTranspose1d(in_channels=32, out_channels=64,kernel_size=2, padding=0),
            nn.ReLU(inplace=True),
            # nn.ConstantPad1d( padding=(0,1), value = 0),
            #nn.Tanh()
        )
        self.unpool3 = nn.MaxUnpool1d(kernel_size=2, padding=0)
        self.deconv3 = nn.Sequential(
            nn.ConvTranspose1d(in_channels=64, out_channels=1,kernel_size=2, padding=0),
            nn.ReLU(inplace=True),
            nn.ConstantPad1d( padding=(0,1), value = 0),
            nn.Tanh()
        )
       
        self.encoded = nn.Sequential(
            nn.Conv1d(in_channels=1, out_channels=64, kernel_size=2, padding=0),
            nn.ReLU(inplace=True),
            nn.MaxPool1d(kernel_size=2, padding=0),
            nn.Conv1d(in_channels=64, out_channels=32, kernel_size=2, padding=0),
            nn.ReLU(inplace=True),
            nn.MaxPool1d(kernel_size=2, padding=0),
            nn.Conv1d(in_channels=32, out_channels=1, kernel_size=2, padding=0),
            nn.ReLU(inplace=True),
            nn.MaxPool1d(kernel_size=2, padding=0)  
        )

    def forward(self, x):
        x, indices1 = self.conv1(x)
        x, indices2 = self.conv2(x)
        encoded, indices3 = self.conv3(x)
        out = self.unpool1(encoded,indices3)
        out = self.deconv1(out)
        out = self.unpool2(out, indices2)
        out = self.deconv2(out)
        out = self.unpool3(out, indices1)
        decoded = self.deconv3(out)
        return decoded

# %% helper functions
def detect_active_regions(image_path):
    global active_region_path_directory
    global threshold_value
    if(os.path.exists(image_path)):
        # load the image, convert it to grayscale, and blur it
        image = cv2.imread(image_path)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        blurred = cv2.GaussianBlur(gray, (11, 11), 0)
        thresh = cv2.threshold(blurred, threshold_value,255, cv2.THRESH_BINARY)[1]
        thresh = cv2.erode(thresh, None, iterations=2)
        thresh = cv2.dilate(thresh, None, iterations=4)
        file_path = "{active_region_file_path}/{base_file_name}_AR.jpg".format(active_region_file_path=active_region_path_directory, base_file_name=os.path.basename(image_path))
        plt.imsave(file_path, thresh)
        thresh = thresh / 255        
        print(file_path)
        return thresh

def divide_by_grid_chunks(image_tensor, grid_size_x, grid_size_y):
    chunks = []
    rows = torch.chunk(image_tensor, grid_size_x, 0)
    for row in rows:
        cols = torch.chunk(row, grid_size_y, 1)
        for col in cols:
            chunks.append(col)
    return chunks

def sum_ar_in_chunks(grid_chunks):
    sums_in_chunks = []
    for chunk in grid_chunks:
        num_pixels = torch.sum((chunk > 0))
        sums_in_chunks.append(num_pixels.item())
    return sums_in_chunks

def get_descriptor(ar_image_matrix, grid_size_x, grid_size_y, device):
    ar_image_matrix_tensor = torch.tensor(ar_image_matrix, device=device)
    grid_chunks = divide_by_grid_chunks(ar_image_matrix_tensor, grid_size_x, grid_size_y)
    sums_in_chunks = sum_ar_in_chunks(grid_chunks)
    return sums_in_chunks
 
def save_encoded_hash(descriptor, file_path):
    np_desc= descriptor.cpu().detach().numpy()
    with open(file_path, "a") as file:
        desc_str = ','.join(str(x) for x in np_desc)
        file.write("{desc}\n".format(desc=desc_str))
    

if __name__ == '__main__':
    try:
        is_cuda_available = torch.cuda.is_available()
        device = torch.device("cuda") if torch.cuda.is_available() else torch.device("cpu")
        max_descriptor_value= 0
        threads_no = 15
        print("Device: {val}".format(val=device))
        print("CPU threads numer: {threads_no}".format(threads_no=threads_no))
#%% Active region detection 
        imagePaths = ["{directory}/{x}".format(x=x, directory = main_image_directory)
                      for x in os.listdir(main_image_directory) if x.endswith(".jpg")]
        pool = Pool(threads_no)
        freeze_support()        
        start = time.time()
        active_region_detected_images = pool.map(detect_active_regions, imagePaths)
        pool.close()
        pool.join()
#%% Generate descriptors
        descriptors = []
        grid_num_x = 10 
        grid_num_y = 10
        for active_region_detected_image in active_region_detected_images:
            descriptor = get_descriptor(active_region_detected_image,grid_num_x, grid_num_y, device)
            descriptor_max = max(descriptor)
            max_descriptor_value = descriptor_max if descriptor_max > max_descriptor_value else max_descriptor_value 
            descriptors.append(descriptor)
# %% run autoencoder learning
        num_epochs = 10
        # batch_size = 30
        learning_rate = 1e-3
        torch.cuda.empty_cache()
        model = autoencoder().cuda()        
        summary(model, (1,100))
        if os.path.exists("model.pdf"):
            os.remove("model.pdf")
        criterion = nn.MSELoss()
        optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate, weight_decay=1e-5)
        for epoch in range(num_epochs):
            torch.cuda.empty_cache()
            for data in descriptors:
                torch.cuda.empty_cache()
                data_np = np.array(data) / max_descriptor_value
                img = torch.tensor(data_np, dtype=torch.float32).view(1, 1, len(data))
                img = Variable(img).cuda()
                # ===================forward=====================
                output = model(img)
                if not os.path.exists("model.pdf"):
                    dot = make_dot(output.mean(), params=dict(model.named_parameters())) # shows graph
                    dot.render("model", format="pdf", cleanup=True)
                loss = criterion(output, img)
                # ===================backward====================
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
            # ===================log========================
            print('epoch [{}/{}], loss:{:.4f}'.format(epoch+1, num_epochs, loss.data.item()))
        torch.save(model.state_dict(), './conv_autoencoder.pth')
#%% Get encoded hashes        
        encodedModel = model.encoded
        torch.cuda.empty_cache()
        for data in descriptors:
            torch.cuda.empty_cache()
            data_np = np.array(data) / max_descriptor_value
            img = torch.tensor(data_np, dtype=torch.float32).view(1, 1, len(data))
            img = Variable(img).cuda()
            #print(img)
            output_hash = encodedModel(img)
            #print(output)
            save_encoded_hash(output_hash.flatten(), "enc_descriptors.txt")
    except Exception as e:
        logSlack(str(e))
        raise
